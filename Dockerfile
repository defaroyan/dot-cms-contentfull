# Base Image
FROM node:16-alpine

# Working Directory
WORKDIR /app

# Copy package.json and package-lock.json files
COPY package.json ./

# Install dependencies
RUN yarn install

# Copy the rest of the application code
COPY . .

# Build the application
RUN yarn build

# Expose port 8888 for the application
EXPOSE 39209

# Start the application
CMD yarn start
